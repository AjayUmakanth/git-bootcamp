﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exceptions
{
    public class EmptyActorException : Exception

    {
        public EmptyActorException
()
        {

        }
        public EmptyActorException(string msg, Exception innerException)
            : base(msg, innerException)
        {
        }
        public EmptyActorException(string msg)
            : base(String.Format("Actor is empty: {0}", msg))
        {

        }
    }
}
