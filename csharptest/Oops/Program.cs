﻿using System;
using Oops.Services;
using Oops.Models;
using System.Collections.Generic;

namespace Oops
{
    class Program
    {
        
        static void Main(string[] args)
        {
            String[] names = { "ABC", "DEF", "GHI", "JKL", "MNO" };
            Boolean[] present = {true,false,true,true,false};
            String date = "2019-11-2";
            PersonService students = new PersonService();
            for (int i = 0; i < 5; i++)
            {
                Student student = new Student();
                student.Id = 100;
                student.Name = names[i];
                student.Attendance = new List<Attendance>();
                student.Marks = new List<Marks>();
                students.Add(student);
                for (int j = 0; j < 5; j++)
                {
                    student.Marks.Add(new Marks() { name = "Sub " + j, marks = 90 + j });
                    students.MarkAttendance(student, new Attendance() { Date = DateTime.Parse(date + j), Present = present[j] });
                }
            }
            Student s = (Student)students.GetPerson(2);
            foreach (Marks m in s.Marks)
                Console.WriteLine(m.name+": " + m.marks);
        }
    }
}
