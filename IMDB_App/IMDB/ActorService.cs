﻿using System;
using System.Collections.Generic;
using System.Text;
using IMDB.Repository;
using IMDB.Domain;
using Exceptions;
namespace IMDB
{
    public class ActorService
    {

        private ActorRepository _actorRepository = new ActorRepository();
        public void Add(Actor actor)
        {
            if (string.IsNullOrEmpty(actor?.Name))
            {
                throw new EmptyActorException("Name can't be empty");
            }
            _actorRepository.Add(actor);
        }
        public List<Actor> GetActors()
        {
            return _actorRepository.GetActors();
        }
        public Actor GetActor(string name)
        {
            return _actorRepository.GetActor(name);

        }
    }
}

