﻿using System;
using System.Collections.Generic;
using System.Text;
using Oops.Models;
using Oops.Repositories;

namespace Oops.Services
{
    public class PersonService:IPersonService
    {
        private PersonRepository _persons = new PersonRepository();
        public void MarkAttendance(Person person, Attendance attendance)
        {
            person.Attendance.Add(attendance);
        }

       public  List<Attendance> GetAttendance(int id)
        {
            Person person = _persons.GetPerson(id);
            return person.Attendance;
        }
        public Person GetPerson(int id)
        {
            return _persons.GetPerson(id);
        }

        public void Add(Person person)
        {
            _persons.Add(person);
        }
    }
}

