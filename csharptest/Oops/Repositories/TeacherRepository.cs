﻿using Oops.Models;
using System.Collections.Generic;
using System.Linq;

namespace Oops.Repositories
{
    public class TeacherRepository
    {
        private List<Teacher> _teacher = new List<Teacher>();

        public void Add(Teacher person)
        {
            _teacher.Add(person);
        }

        public List<Teacher> GetAllTeachers()
        {
            return _teacher.ToList();
        }

        public Teacher GetTeacher(int id)
        {
            return _teacher.FirstOrDefault(p => p.Id == id);
        }
    }
}
