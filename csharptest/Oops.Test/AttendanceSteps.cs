﻿using System;
using TechTalk.SpecFlow;
using Oops.Models;
using System.Globalization;
using Oops.Services;
using Oops.Models;
using System.Collections.Generic;

namespace Oops.Test
{
    [Binding]
    public class AttendanceSteps
    {
        PersonService persons = new PersonService();
        Person person;
        DateTime date;
        [Given(@"I have a student with Id as ""(.*)""")]
        public void GivenIHaveAStudentWithIdAs(int ID)
        {
            person = persons.GetPerson(ID);
        }

        [When(@"When I mark the student as present for ""(.*)""")]
        public void WhenWhenIMarkTheStudentAsPresentFor(string dateString)
        {
            date = DateTime.Parse(dateString, CultureInfo.InvariantCulture);
            Attendance attendance = new Attendance()
            {
                Date = date,
                Present = true
            };
            persons.MarkAttendance(person, attendance);

        }

        [Then(@"the attendance for the student for the day should be ""(.*)""")]
        public void ThenTheAttendanceForTheStudentForTheDayShouldBe(string mark)
        {
            var attendance = person.Attendance.Find(a => a.Date == date);
            if(attendance == null)
            {
                throw new Exception();
            }
            if (attendance.Present != bool.Parse(mark))
            {
                throw new Exception("Not marked");
            }
        }
        [BeforeScenario]
        [Scope(Tag = "mark")]
        public void AddTestData2()
        {
            persons.Add(new Person()
            {
                Id = 1251515,
                Name = "ajay",
                Attendance=new List<Attendance>()
            });

        }
    }
}
