﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Oops.Models
{
    public class Student:Person
    {
        public List<Marks> Marks { get; set; }
    }
}
