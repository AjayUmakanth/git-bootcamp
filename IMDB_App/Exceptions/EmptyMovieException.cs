﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exceptions
{
    public class EmptyMovieException : Exception

    {
        public EmptyMovieException()
        {

        }
        public EmptyMovieException(string msg, Exception innerException)
            : base(msg, innerException)
        {
        }
        public EmptyMovieException(string msg)
            : base(String.Format("Actor is empty: {0}", msg))
        {

        }
    }
}
