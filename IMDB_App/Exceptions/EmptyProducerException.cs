﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exceptions
{
    public class EmptyProducerException : Exception

    {
        public EmptyProducerException()
        {

        }
        public EmptyProducerException(string msg, Exception innerException)
            : base(msg, innerException)
        {
        }
        public EmptyProducerException(string msg)
            : base(String.Format("Actor is empty: {0}", msg))
        {

        }
    }
}
