﻿Feature: IMDB
	Provide interfaces for users
	To add actors, produducers, movies
	And list and delete movies
@addMovie
Scenario: Add movie
	Given movie title "Ford v Ferrari"
	And year of release "2019"
	And plot as "American car designer Carroll Shelby and driver Ken Miles battle corporate interference, the laws of physics and their own personal demons to build a revolutionary race car for Ford and challenge Ferrari at the 24 Hours of Le Mans in 1966"
	And producer as "James Mangold"
	And actors as "Matt Damon,Christian Bale"
	When movie is added
	Then the result should be 
	| Title          | Year of Release | Plot                                                                                                                                                                                                                                           | Actors                    | Producer      |
	| Ford v Ferrari | 2019            | American car designer Carroll Shelby and driver Ken Miles battle corporate interference, the laws of physics and their own personal demons to build a revolutionary race car for Ford and challenge Ferrari at the 24 Hours of Le Mans in 1966 | Matt Damon,Christian Bale | James Mangold |
	
@listMovies
Scenario: List Movies
	Given List of movies
	When movies are listed
	Then the results should be 
	| Title                    | Year of Release | Plot                                                                                                                                                                                                                                           | Actors         | Producer      |
	| Ford v Ferrari           | 2019            | American car designer Carroll Shelby and driver Ken Miles battle corporate interference, the laws of physics and their own personal demons to build a revolutionary race car for Ford and challenge Ferrari at the 24 Hours of Le Mans in 1966 | Matt Damon     | James Mangold |
	| The Shawshank Redemption | 1993            | Story of banker Andy Dufresne (Tim Robbins), who is sentenced to life in Shawshank State Penitentiary for the murders of his wife and her lover, despite his claims of innocence                                                               | Morgan Freeman | Niki Marvin   |


