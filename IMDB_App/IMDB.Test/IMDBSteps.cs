﻿using System;
using System.Collections.Generic;
using IMDB.Domain;
using Exceptions;
using IMDB.Repository;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using TechTalk.SpecFlow;

namespace IMDB.Test
{

    [Binding]
    public class IMDBSteps
    {

        private static Movie _movie;
        private List<Movie> _moviesList;
        private string _movieTitle = "";
        private static MoviesService _movies;
        private static ActorService _actors;
        private static ProducerService _producer;
        [Given(@"movie title ""(.*)""")]
        public void GivenMovieTitle(string title)
        {
            _movie.Title = title ?? throw new EmptyMovieException("Movie Title is Empty");
            _movieTitle = title;
        }

        [Given(@"year of release ""(.*)""")]
        public static void GivenYearOfRelease(int year)
        {
            _movie.YearOfRelease = year;
        }

        [Given(@"plot as ""(.*)""")]
        public static void GivenPlotAs(string plot)
        {
            _movie.Plot = plot ?? throw new EmptyMovieException("Movie plot empty");
        }

        [Given(@"actors as ""(.*)""")]
        public static void GivenActorsAs(string actors)
        {
            if (actors == null)
                throw new EmptyActorException("Actor name is null");
            _movie.Actors = new List<Actor>();
            foreach (string actor in actors.Split(','))
                _movie.Actors.Add(_actors.GetActor(actor));
        }

        [Given(@"producer as ""(.*)""")]
        public static void GivenProducerAs(string producer)
        {
            if (producer == null)
                throw new EmptyProducerException("Producer name is empty");
            _movie.Producer = _producer.GetProducer(producer);
        }

        [Given(@"List of movies")]
        public static void GivenListOfMovies()
        {
        }

        [When(@"movie is added")]
        public static void WhenMovieIsAdded()
        {
            _movies.Add(_movie);
        }

        [When(@"movies are listed")]
        public void WhenMoviesAreListed()
        {
            _moviesList = _movies.GetMovies();
        }
        public static Movie CreateMovie(TableRow row)
        {
            if (row == null)
                throw new EmptyMovieException("No data in table");
            Movie tempPovie = new Movie
            {
                Title = row["Title"],
                YearOfRelease = int.Parse(row["Year of Release"]),
                Plot = row["Plot"],
                Producer = _producer.GetProducer(row["Producer"]),
                Actors = new List<Actor>()
            };
            foreach (string actor in row["Actors"].Split(','))
                tempPovie.Actors.Add(_actors.GetActor(actor));
            return tempPovie;
        }
        [Then(@"the result should be")]
        public void ThenTheResultShouldBe(Table table)
        {
            Movie addedMovie = _movies.GetMovie(_movieTitle);
            var row = table.Rows[0];
            Movie toCompare = CreateMovie(row);
            if (JsonConvert.SerializeObject(addedMovie) != JsonConvert.SerializeObject(toCompare))
                throw new EmptyMovieException("Movie added");
        }

        [Then(@"the results should be")]
        public void ThenTheResultsShouldBe(Table table)
        {
            if (table == null)
                throw new EmptyMovieException("Movie table is empty");
            for (int i = 0; i < _moviesList.Count; i++)
                if (JsonConvert.SerializeObject(CreateMovie(table.Rows[i])) != JsonConvert.SerializeObject(_moviesList[i]))
                    throw new EmptyMovieException("Movies not present");
        }


        [BeforeScenario]
        [Scope(Tag = "addMovie")]
        public static void AddTestData()
        {
            _movie = new Movie();
            _movies = new MoviesService();
            _actors = new ActorService();
            _producer = new ProducerService();
            _actors.GetActors().Clear();
            _producer.GetProducers().Clear();
            _movies.GetMovies().Clear();
            _actors.Add(new Actor()
            {
                Name = "Matt Damon",
                DateOfBirth = "8/10/1970"
            });
            _actors.Add(new Actor()
            {
                Name = " Christian Bale",
                DateOfBirth = " 30/1/1974"
            });
            _producer.GetProducers().Clear();
            _producer.Add(new Producer()
            {
                Name = "James Mangold",
                DateOfBirth = "8/10/1970"
            });

        }
        [BeforeScenario]
        [Scope(Tag = "listMovies")]
        public static void AddTestData2()
        {
            _movie = new Movie();
            _movies = new MoviesService();
            _actors = new ActorService();
            _producer = new ProducerService();
            _actors.GetActors().Clear();
            _producer.GetProducers().Clear();
            _movies.GetMovies().Clear();
            _actors.Add(new Actor()
            {
                Name = "Matt Damon",
                DateOfBirth = "8/10/1970"
            });
            _actors.Add(new Actor()
            {
                Name = "Morgan Freeman",
                DateOfBirth = " 30/1/1974"
            });
            _producer.GetProducers().Clear();
            _producer.Add(new Producer()
            {
                Name = "James Mangold",
                DateOfBirth = "8/10/1970"
            });
            _producer.Add(new Producer()
            {
                Name = "Niki Marvin",
                DateOfBirth = "8/10/1970"
            });
            _movies.Add(new Movie()
            {
            Title = "Ford v Ferrari",
            YearOfRelease = 2019,
            Plot = "American car designer Carroll Shelby and driver Ken Miles battle corporate interference, the laws of physics and their own personal demons to build a revolutionary race car for Ford and challenge Ferrari at the 24 Hours of Le Mans in 1966",
            Actors=new List<Actor>() { _actors.GetActor("Matt Damon") },
            Producer=_producer.GetProducer("James Mangold")
            });
            _movies.Add(new Movie()
            {
                Title = "The Shawshank Redemption",
                YearOfRelease = 1993,
                Plot = "Story of banker Andy Dufresne (Tim Robbins), who is sentenced to life in Shawshank State Penitentiary for the murders of his wife and her lover, despite his claims of innocence",
                Actors = new List<Actor>() { _actors.GetActor("Morgan Freeman") },
                Producer = _producer.GetProducer("Niki Marvin")
            });
        }
    }
}
