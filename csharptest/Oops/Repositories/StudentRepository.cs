﻿using Oops.Models;
using System.Collections.Generic;
using System.Linq;

namespace Oops.Repositories
{
    public class StudentRepository
    {
        private List<Student> _students = new List<Student>();

        public void Add(Student student)
        {
            _students.Add(student);
        }

        public List<Student> GetAllStudents()
        {
            return _students;
        }

        public Student GetStudent(int id)
        {
            return _students.FirstOrDefault(p => p.Id == id);
        }
    }
}
