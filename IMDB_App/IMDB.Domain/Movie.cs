﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace IMDB.Domain
{
    public class Movie
    {
        public string Title { get; set; }
        public int YearOfRelease { get; set; }
        public string Plot { get; set; }

        public List<Actor> Actors { get; set; }
        public Producer Producer { get; set; }
    }
}
