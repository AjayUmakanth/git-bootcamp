﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace IMDB.Domain
{
    public class Actor
    {
        public string Name { get; set; }
        public string DateOfBirth { get; set; }
    }
}
