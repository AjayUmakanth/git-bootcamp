﻿using System;
using System.Collections.Generic;
using System.Text;
using IMDB.Repository;
using IMDB.Domain;
using Exceptions;
namespace IMDB
{
    public class MoviesService
    {

        private MovieRepository _movieRepository = new MovieRepository();
        public void Add(Movie movie)
        {
            if (string.IsNullOrEmpty(movie?.Title))
            {
                throw new EmptyMovieException("Title can't be empty");
            }
            if (movie?.Producer==null)
            {
                throw new EmptyProducerException("Producer not present");
            }
            if (movie.Actors == null)
            {
                throw new EmptyActorException("Actor not present");
            }
            if(movie.YearOfRelease> (int)DateTime.Now.Year)
            {
                throw new EmptyMovieException("Invalid year");
            }
            _movieRepository.Add(movie);
        }
        public List<Movie> GetMovies()
        {
            return _movieRepository.GetMovies();
        }
        public Movie GetMovie(string title)
        {
            return _movieRepository.GetMovie(title);

        }
        public void RemoveMovie(int index)
        {
            if (index < 0 || index >= _movieRepository.GetCount())
                throw new EmptyMovieException("Movie not present");
            else
                _movieRepository.RemoveMovie(index);
        }
    }
}

