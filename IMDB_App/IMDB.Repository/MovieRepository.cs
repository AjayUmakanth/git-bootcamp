﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMDB.Domain;

 namespace IMDB.Repository
{
    public class MovieRepository
    {
        private List<Movie> _movies = new List<Movie>();
        public void Add(Movie movie)
        {
            _movies.Add(movie);
        }
        public List<Movie> GetMovies()
        {
            return _movies.ToList();
        }
        public Movie GetMovie(string title)
        {
            return _movies.FirstOrDefault(m => m.Title == title);

        }
        public void RemoveMovie(int index)
        {
            _movies.RemoveAt(index);
        }
        public int GetCount()
        {
            return _movies.ToList().Count;
        }
    }
}
