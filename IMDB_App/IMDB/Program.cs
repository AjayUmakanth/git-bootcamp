﻿using System;
using System.Collections.Generic;
using System.Globalization;
using IMDB.Domain;

namespace IMDB
{
    class Program
    {
        static public ProducerService producer;
        static public ActorService actor;
        static public MoviesService movies;
        public static void ListMovies()
        {
            List<Movie> list = movies.GetMovies();
            foreach(Movie m in list)
            {
                Console.WriteLine("{0}({1})",m.Title,m.YearOfRelease);
                Console.Write("Plot-"+m.Plot+"\nActors-");
                foreach(Actor a in m.Actors)
                    Console.Write(a.Name+" ");
                Console.WriteLine("\nProducer-" + m.Producer.Name+"\n\n");
            }
        }
        public static void AddActor()
        {
            Actor newActor = new Actor();
            Console.Write("Enter Name:");
            newActor.Name= Console.ReadLine();
            Console.Write("Enter DOB:");
            newActor.DateOfBirth = Console.ReadLine();
            actor.Add(newActor);
            Console.Write("Actor Added");
        }
        public static void AddProducer()
        {
            Producer newProducer = new Producer();
            Console.Write("Enter Name:");
            newProducer.Name = Console.ReadLine();
            Console.Write("Enter DOB:");
            newProducer.DateOfBirth = Console.ReadLine();
            producer.Add(newProducer);
            Console.Write("Producer Added");
        }
        public static void AddMovies()
        {
            List<Actor> alist = actor.GetActors();
            List<Producer> plist = producer.GetProducers();
            Movie newMovie = new Movie();
            Console.Write("Enter Title:");
            newMovie.Title = Console.ReadLine();
            Console.Write("Enter Year of Release:");
            newMovie.YearOfRelease = int.Parse(Console.ReadLine(), CultureInfo.InvariantCulture);
            Console.Write("Enter Plot:");
            newMovie.Plot = Console.ReadLine();
            Console.Write("Choose Actor:");
            for(int i=0;i<alist.Count;i++)
                Console.Write("{0}.{1} ", (i + 1), alist[i].Name);
            int[] choices = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);
            newMovie.Actors = new List<Actor>();
            foreach (int choice in choices)
                newMovie.Actors.Add(alist[choice-1]);
            Console.Write("Choose Producer:");
            for (int i = 0; i < plist.Count; i++)
                Console.Write("{0}.{1} ",(i + 1),plist[i].Name);
            int choice2 = int.Parse(Console.ReadLine(), CultureInfo.InvariantCulture);
            newMovie.Producer=plist[choice2-1];
            movies.Add(newMovie);
            Console.Write("Movie Added");
        }
        public static void DeleteMovie()
        {
            List<Movie> list = movies.GetMovies();
            for (int i = 0; i < list.Count; i++)
                Console.Write("{0} {1}",i,list[i].Title);
            Console.Write("Enter Choice");
            int choice= int.Parse(Console.ReadLine(), CultureInfo.InvariantCulture);
            movies.RemoveMovie(choice);
        }
        static void Main()
        {
            movies = new MoviesService();
            actor = new ActorService();
            producer = new ProducerService();
            int option = 0;
            while (option != 6)
            {
                Console.WriteLine("1)List Movies\n2)Add Movie\n3)Add Actor\n4)Add Producer\n5)Delete Movie\n6)Exit");
                option = int.Parse(Console.ReadLine(), CultureInfo.InvariantCulture);
                Console.WriteLine(option);
                switch(option)
                {
                    case 1: ListMovies();break;
                    case 2: AddMovies(); break;
                    case 3: AddActor(); break;
                    case 4: AddProducer(); break;
                    case 5: DeleteMovie(); break;
                    case 6: Console.WriteLine("Thank you, signing off!!"); break;
                    default:Console.WriteLine("Invalid Option");break;
                }
            }
        }
    }
}
