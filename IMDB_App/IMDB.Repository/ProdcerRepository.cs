﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMDB.Domain;

namespace IMDB.Repository
{
    public class ProducerRepository
    {
        private List<Producer> _producers = new List<Producer>();
        public void Add(Producer producer)
        {
            _producers.Add(producer);
        }
        public List<Producer> GetProducers()
        {
            return _producers.ToList();
        }

        public Producer GetProducer(string name)
        {
            return _producers.FirstOrDefault(p => p.Name == name);

        }
    }
}
