﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMDB.Domain;

namespace IMDB.Repository
{
    public class ActorRepository
    {
        private List<Actor> _actors = new List<Actor>();
        public void Add(Actor actor)
        {
            _actors.Add(actor);
        }
        public List<Actor> GetActors()
        {
            return _actors.ToList();
        }
        public Actor GetActor(string name)
        {
            return _actors.FirstOrDefault(a => a.Name == name);

        }

    }
}

