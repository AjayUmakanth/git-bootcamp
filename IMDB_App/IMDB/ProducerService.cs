﻿using System;
using System.Collections.Generic;
using System.Text;
using IMDB.Repository;
using IMDB.Domain;
using Exceptions;

namespace IMDB
{
    public class ProducerService
    {

        private ProducerRepository _producerRepository = new ProducerRepository();
        public void Add(Producer producer)
        {
            if (string.IsNullOrEmpty(producer?.Name))
            {
                throw new EmptyProducerException("Name can't be empty");
            }
            _producerRepository.Add(producer);
        }
        public List<Producer> GetProducers()
        {
            return _producerRepository.GetProducers();
        }
        public Producer GetProducer(string name)
        {
            return _producerRepository.GetProducer(name);

        }
    }
}

