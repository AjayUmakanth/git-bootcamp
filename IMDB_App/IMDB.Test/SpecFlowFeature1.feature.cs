// ------------------------------------------------------------------------------
//  <auto-generated>
//      This code was generated by SpecFlow (http://www.specflow.org/).
//      SpecFlow Version:3.1.0.0
//      SpecFlow Generator Version:3.1.0.0
// 
//      Changes to this file may cause incorrect behavior and will be lost if
//      the code is regenerated.
//  </auto-generated>
// ------------------------------------------------------------------------------
#region Designer generated code
#pragma warning disable
namespace IMDB.Test
{
    using TechTalk.SpecFlow;
    using System;
    using System.Linq;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("TechTalk.SpecFlow", "3.1.0.0")]
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public partial class IMDBFeature : object, Xunit.IClassFixture<IMDBFeature.FixtureData>, System.IDisposable
    {
        
        private static TechTalk.SpecFlow.ITestRunner testRunner;
        
        private string[] _featureTags = ((string[])(null));
        
        private Xunit.Abstractions.ITestOutputHelper _testOutputHelper;
        
#line 1 "SpecFlowFeature1.feature"
#line hidden
        
        public IMDBFeature(IMDBFeature.FixtureData fixtureData, IMDB_Test_XUnitAssemblyFixture assemblyFixture, Xunit.Abstractions.ITestOutputHelper testOutputHelper)
        {
            this._testOutputHelper = testOutputHelper;
            this.TestInitialize();
        }
        
        public static void FeatureSetup()
        {
            testRunner = TechTalk.SpecFlow.TestRunnerManager.GetTestRunner();
            TechTalk.SpecFlow.FeatureInfo featureInfo = new TechTalk.SpecFlow.FeatureInfo(new System.Globalization.CultureInfo("en-US"), "IMDB", "\tProvide interfaces for users\r\n\tTo add actors, produducers, movies\r\n\tAnd list and" +
                    " delete movies", ProgrammingLanguage.CSharp, ((string[])(null)));
            testRunner.OnFeatureStart(featureInfo);
        }
        
        public static void FeatureTearDown()
        {
            testRunner.OnFeatureEnd();
            testRunner = null;
        }
        
        public virtual void TestInitialize()
        {
        }
        
        public virtual void TestTearDown()
        {
            testRunner.OnScenarioEnd();
        }
        
        public virtual void ScenarioInitialize(TechTalk.SpecFlow.ScenarioInfo scenarioInfo)
        {
            testRunner.OnScenarioInitialize(scenarioInfo);
            testRunner.ScenarioContext.ScenarioContainer.RegisterInstanceAs<Xunit.Abstractions.ITestOutputHelper>(_testOutputHelper);
        }
        
        public virtual void ScenarioStart()
        {
            testRunner.OnScenarioStart();
        }
        
        public virtual void ScenarioCleanup()
        {
            testRunner.CollectScenarioErrors();
        }
        
        void System.IDisposable.Dispose()
        {
            this.TestTearDown();
        }
        
        [Xunit.SkippableFactAttribute(DisplayName="Add movie")]
        [Xunit.TraitAttribute("FeatureTitle", "IMDB")]
        [Xunit.TraitAttribute("Description", "Add movie")]
        [Xunit.TraitAttribute("Category", "addMovie")]
        public virtual void AddMovie()
        {
            string[] tagsOfScenario = new string[] {
                    "addMovie"};
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Add movie", null, new string[] {
                        "addMovie"});
#line 6
this.ScenarioInitialize(scenarioInfo);
#line hidden
            bool isScenarioIgnored = default(bool);
            bool isFeatureIgnored = default(bool);
            if ((tagsOfScenario != null))
            {
                isScenarioIgnored = tagsOfScenario.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((this._featureTags != null))
            {
                isFeatureIgnored = this._featureTags.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((isScenarioIgnored || isFeatureIgnored))
            {
                testRunner.SkipScenario();
            }
            else
            {
                this.ScenarioStart();
#line 7
 testRunner.Given("movie title \"Ford v Ferrari\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
#line 8
 testRunner.And("year of release \"2019\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 9
 testRunner.And("plot as \"American car designer Carroll Shelby and driver Ken Miles battle corpora" +
                        "te interference, the laws of physics and their own personal demons to build a re" +
                        "volutionary race car for Ford and challenge Ferrari at the 24 Hours of Le Mans i" +
                        "n 1966\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 10
 testRunner.And("producer as \"James Mangold\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 11
 testRunner.And("actors as \"Matt Damon,Christian Bale\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 12
 testRunner.When("movie is added", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line hidden
                TechTalk.SpecFlow.Table table1 = new TechTalk.SpecFlow.Table(new string[] {
                            "Title",
                            "Year of Release",
                            "Plot",
                            "Actors",
                            "Producer"});
                table1.AddRow(new string[] {
                            "Ford v Ferrari",
                            "2019",
                            "American car designer Carroll Shelby and driver Ken Miles battle corporate interf" +
                                "erence, the laws of physics and their own personal demons to build a revolutiona" +
                                "ry race car for Ford and challenge Ferrari at the 24 Hours of Le Mans in 1966",
                            "Matt Damon,Christian Bale",
                            "James Mangold"});
#line 13
 testRunner.Then("the result should be", ((string)(null)), table1, "Then ");
#line hidden
            }
            this.ScenarioCleanup();
        }
        
        [Xunit.SkippableFactAttribute(DisplayName="List Movies")]
        [Xunit.TraitAttribute("FeatureTitle", "IMDB")]
        [Xunit.TraitAttribute("Description", "List Movies")]
        [Xunit.TraitAttribute("Category", "listMovies")]
        public virtual void ListMovies()
        {
            string[] tagsOfScenario = new string[] {
                    "listMovies"};
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("List Movies", null, new string[] {
                        "listMovies"});
#line 18
this.ScenarioInitialize(scenarioInfo);
#line hidden
            bool isScenarioIgnored = default(bool);
            bool isFeatureIgnored = default(bool);
            if ((tagsOfScenario != null))
            {
                isScenarioIgnored = tagsOfScenario.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((this._featureTags != null))
            {
                isFeatureIgnored = this._featureTags.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((isScenarioIgnored || isFeatureIgnored))
            {
                testRunner.SkipScenario();
            }
            else
            {
                this.ScenarioStart();
#line 19
 testRunner.Given("List of movies", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
#line 20
 testRunner.When("movies are listed", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line hidden
                TechTalk.SpecFlow.Table table2 = new TechTalk.SpecFlow.Table(new string[] {
                            "Title",
                            "Year of Release",
                            "Plot",
                            "Actors",
                            "Producer"});
                table2.AddRow(new string[] {
                            "Ford v Ferrari",
                            "2019",
                            "American car designer Carroll Shelby and driver Ken Miles battle corporate interf" +
                                "erence, the laws of physics and their own personal demons to build a revolutiona" +
                                "ry race car for Ford and challenge Ferrari at the 24 Hours of Le Mans in 1966",
                            "Matt Damon",
                            "James Mangold"});
                table2.AddRow(new string[] {
                            "The Shawshank Redemption",
                            "1993",
                            "Story of banker Andy Dufresne (Tim Robbins), who is sentenced to life in Shawshan" +
                                "k State Penitentiary for the murders of his wife and her lover, despite his clai" +
                                "ms of innocence",
                            "Morgan Freeman",
                            "Niki Marvin"});
#line 21
 testRunner.Then("the results should be", ((string)(null)), table2, "Then ");
#line hidden
            }
            this.ScenarioCleanup();
        }
        
        [System.CodeDom.Compiler.GeneratedCodeAttribute("TechTalk.SpecFlow", "3.1.0.0")]
        [System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
        public class FixtureData : System.IDisposable
        {
            
            public FixtureData()
            {
                IMDBFeature.FeatureSetup();
            }
            
            void System.IDisposable.Dispose()
            {
                IMDBFeature.FeatureTearDown();
            }
        }
    }
}
#pragma warning restore
#endregion
